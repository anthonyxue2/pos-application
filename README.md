# SAXPOS POS: Point of Sales Application
## The 'Universal' Point of Sales Application

The Universal Point of Sales application that can be run on any platform.

This project combines the Electron Framework, Ionic 4 and Angular that can run on either the desktop (macOS, Windows and Linux), a browser or mobile devices (iOS, Android and Windows Phone).

# Quick start
Make sure you have node installed and running, then install Ionic and Cordova globally using npm.
```
npm install -g ionic@latest cordova
```
Clone the repo, install the npm packages and run the Electron app
```
git clone git clone https://anthonyxue2@bitbucket.org/anthonyxue2/pos-application.git
cd pos-application
npm install
npm start
```
# Ionic CLI Commands
```
ionic serve
ionic serve --lab
```
# Electron CLI Commands
```
electron .
```