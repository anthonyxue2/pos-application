import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private storage: Storage, private auth: AuthService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if(this.auth.isAuthenticated()){
      return true;
    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }

  public set(settingName,value){
    return this.storage.set(settingName,value);
  }
  public async get(settingName){
    return await this.storage.get(settingName);
  }
  public async remove(settingName){
    return await this.storage.remove(settingName);
  }
  public clear() {
    this.storage.clear().then(() => {
      console.log('all keys cleared');
    });
  }

}
