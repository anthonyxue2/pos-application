import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch:'full' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate: [AuthGuard] },
  { path: 'order', loadChildren: './pages/order/order.module#OrderPageModule', canActivate: [AuthGuard] },
  { path: 'order/:id', loadChildren: './pages/order/order.module#OrderPageModule', canActivate: [AuthGuard] },
  { path: 'payment/:id', loadChildren: './pages/payment/payment.module#PaymentPageModule', canActivate: [AuthGuard] },
  { path: '**', redirectTo: '/home', pathMatch:'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
