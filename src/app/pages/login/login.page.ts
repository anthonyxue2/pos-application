import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  login = { username: '', password: '' };
  submitted = false;


  constructor(public router: Router, 
    private auth: AuthService) { }

  ngOnInit() {
  }

  onLogin() {
    this.submitted = true;
    this.auth.login(this.login.username, this.login.password);
  }

  onSignup() {
    this.router.navigateByUrl('/signup');
  }

}
