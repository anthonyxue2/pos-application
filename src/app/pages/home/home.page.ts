import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TableModel } from '../../models/table.model'
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  homeSegment = "orders";

  constructor(public router: Router,
    private auth: AuthService) {}

  ngOnInit() {
  }
  
  showOrder(event, id){
    event.stopPropagation();
    this.router.navigateByUrl('/order/' + id);
  }

  onPayment(event, id){
    event.stopPropagation();
    this.router.navigateByUrl('/payment/' + id);
  }

  doSignOut(){
    this.auth.logout();
  }

  tables = [{ id: 1, tableName: 'Table 1', noGuests: '1', orderID: 1, invoiceTotal: 100, status: 'IN-USE' },
    { id: 2, tableName: 'Table 2', noGuests: '1', orderID: 1, invoiceTotal: 52.33, status: 'OPEN' },
    { id: 3, tableName: 'Table 3', noGuests: '1', orderID: 1, invoiceTotal: 32.54, status: 'IN-USE' },
    { id: 4, tableName: 'Table 4', noGuests: '1', orderID: 1, invoiceTotal: 12.34, status: 'OPEN' },
    { id: 5, tableName: 'Table 5', noGuests: '1', orderID: 1, invoiceTotal: 50.00, status: 'IN-USE' },
    { id: 6, tableName: 'Table 6', noGuests: '1', orderID: 1, invoiceTotal: 90.45, status: 'OPEN' },
    { id: 7, tableName: 'Table 7', noGuests: '1', orderID: 1, invoiceTotal: 43.55, status: 'IN-USE' },
    { id: 8, tableName: 'Table 8', noGuests: '1', orderID: 1, invoiceTotal: 10.12, status: 'OPEN' },
    { id: 9, tableName: 'Table 9', noGuests: '1', orderID: 1, invoiceTotal: 40.45, status: 'IN-USE' },
    { id: 10, tableName: 'Table 10', noGuests: '1', orderID: 1, invoiceTotal: 80.10, status: 'OPEN' },
    { id: 11, tableName: 'Table 11', noGuests: '1', orderID: 1, invoiceTotal: 100.30, status: 'IN-USE' },
    { id: 12, tableName: 'Table 12', noGuests: '1', orderID: 1, invoiceTotal: 10.50, status: 'OPEN' },
    { id: 13, tableName: 'Table 13', noGuests: '1', orderID: 1, invoiceTotal: 100, status: 'IN-USE' },
    { id: 14, tableName: 'Table 14', noGuests: '1', orderID: 1, invoiceTotal: 101.12, status: 'OPEN' },
    { id: 15, tableName: 'Table 15', noGuests: '1', orderID: 1, invoiceTotal: 45.45, status: 'IN-USE' }];

}
