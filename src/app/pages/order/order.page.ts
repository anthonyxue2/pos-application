import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderModel } from '../../models/order.model';
import { OrderItemModel } from '../../models/orderItem.model';
import { GroupModel } from '../../models/group.model';
import { ProductModel } from '../../models/product.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {
  id;
  orderSegment = "products";
  public selectedGroupID: number;
  public categories: Array<GroupModel> = [];
  public products: Array<ProductModel> = [];
  public orderItems: Array<OrderItemModel> = [];
  public order = new OrderModel();
  public totalQuantity: number;

  constructor(private route: ActivatedRoute,
    public router: Router) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.initialisation();
    this.selectedGroupID = this.categories[0].id;
  }

  toggleGroup(group: GroupModel) {
    if (group.id) {
      this.selectedGroupID = group.id;
    }
  }

  productSelected(product: ProductModel){
    //TODO: Add Product as an orderItem
    this.addProduct(product);
  }

  addProduct(product: ProductModel){

    // Checks to see if orderItem exists in orderItem list
    let found = false;
    for (let x of this.orderItems) {
      if (x.productID == product.id) {
        x.quantity += 1;
        x.totalPrice = x.price * x.quantity;
        found = true;
        break;
      }
    }

    // Otherwise create new orderItem
    if (found == false) {
      let orderItem = new OrderItemModel();
      orderItem.orderID = +this.id;
      orderItem.productID = product.id;
      orderItem.productName = product.name;
      orderItem.price = product.price;
      orderItem.quantity = 1;
      orderItem.totalPrice = orderItem.price;
      this.orderItems.push(orderItem);
    }

    console.log(this.orderItems);
    
    this.increaseQuantity();
  }

  increaseQuantity(){
    if(this.totalQuantity >= 0){
      this.totalQuantity += 1;
    } else {
      this.totalQuantity = 1;
    }
  }

  onSubmit(){
    this.router.navigateByUrl('/home');
  }
  
  // Temporarily there until db is sorted out
  initialisation(){
    let num;
    let k;
    for (num=0;num<=20;num++){
      let tmp = new GroupModel();
      tmp.id = num;
      tmp.name = 'Group ' + num;
      this.categories.push(tmp);
      for (k=0;k<=5;k++){
        let tmp1 = new ProductModel();
        tmp1.id = k;
        tmp1.name = 'Product ' + k;
        tmp1.price = 1;
        tmp1.groupID = tmp.id;
        this.products.push(tmp1);
      }
    }
  }

}
