import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  id;
  amountDue = 50;
  paid = '';
  change = 0;

  constructor(private route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
  }

  onPayment(){
    this.router.navigateByUrl('/home');
  }

  buttonClicked(btn){
    if (btn == 'PAY') {
      if(this.paid == '' || this.change >= 0) {
        this.onPayment();
      } else {
        //INSERT ERROR HERE
      }
    } else if (btn == 'CLEAR') {
      this.paid = '';
    } else if (btn == 'BS') {
      this.paid = this.paid.slice(0, -1);
    } else if (btn == '.') {
      // Checks to see if decimal place exists in string
      if (!this.objectContains(this.paid, btn)){
        this.paid += btn;        
      }
    } else {      
      this.paid += btn;
    }
    this.calculateChange();
  }

  calculateChange(){
    var x = +this.paid;
    this.change = -1 * (this.amountDue - x);
  }

 objectContains(obj, term: string): boolean {
    for (let key in obj){
        if(obj[key].indexOf(term) != -1) return true;
    }
    return false;
  }

}
