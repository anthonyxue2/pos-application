import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { Platform } from '@ionic/angular';

const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState = new BehaviorSubject(false);

  constructor(public router: Router, 
    private storage: Storage, private plt: Platform) {
        this.plt.ready().then(() => {
          this.checkToken();
        })
     }

  public login(username, password){
    return this.storage.set(TOKEN_KEY, username).then(res => {
      this.authState.next(true);
      this.router.navigateByUrl('/home');
    });
  }
  
  public logout() {
    return this.storage.remove(TOKEN_KEY).then(res=> {
      this.authState.next(false);
      this.router.navigateByUrl('/login');
    });

  }

  public isAuthenticated(){
    return this.authState.value;
  }

  public checkToken(){
    return this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authState.next(true);
      }
    });
  }

}
