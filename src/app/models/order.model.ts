export class OrderModel {
    id: number;
    tableID: number;
    type: string;
    status: string;
    total: number;
}
