export class BusinessModel {
    id: number;
    businessName: string;
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    state: string;
    postCode: string;
    phone: string;
}
