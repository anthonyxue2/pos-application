export class ProductModel {
    id: number;
    groupID: number;
    name: string;
    price: number;
}
