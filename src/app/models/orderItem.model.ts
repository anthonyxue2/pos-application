export class OrderItemModel {
    id: number;
    orderID: number;
    tableID: number;
    productID: number;
    productName: string;
    groupID: number;
    price: number;
    totalPrice: number;
    quantity: number;
}
