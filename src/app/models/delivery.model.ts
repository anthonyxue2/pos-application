export class DeliveryModel {
    id: number;
    orderID: number;
    driverID: number;
    customerID: number;
    status: string;
    deliveryFee: number;
}
