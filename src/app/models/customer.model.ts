export class CustomerModel {
    id: number;
    firstName: string;
    lastName: string;
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    state: string;
    postCode: string;
    phone: string;
}
