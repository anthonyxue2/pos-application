export class TableModel {
    id: number;
    tableName: string;
    noGuests: number;
    orderID: number;
    invoiceTotal: number;
    status: string;
}
